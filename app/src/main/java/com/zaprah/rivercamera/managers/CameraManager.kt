package com.zaprah.rivercamera.managers

import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.media.MediaScannerConnection
import android.net.Uri
import android.util.Log
import android.webkit.MimeTypeMap
import android.widget.Toast
import androidx.camera.core.*
import androidx.camera.lifecycle.ProcessCameraProvider
import androidx.camera.video.*
import androidx.camera.video.VideoCapture
import androidx.core.content.ContextCompat
import androidx.core.net.toFile
import androidx.core.util.Consumer
import androidx.lifecycle.LifecycleOwner
import com.zaprah.rivercamera.BuildConfig
import com.zaprah.rivercamera.R
import com.zaprah.rivercamera.enums.FlashState
import com.zaprah.rivercamera.enums.SupportedCamera
import com.zaprah.rivercamera.utils.Constants.Companion.LOG_TAG
import java.io.File
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

object CameraManager {

    private var imageCapture: ImageCapture? = null
    private var camera : Camera? = null

    var outputDirectory: File? = null
    private var cameraExecutor: ExecutorService = Executors.newSingleThreadExecutor()

    var flashState: FlashState = FlashState.off
    var lensFacing = CameraSelector.LENS_FACING_BACK

    private const val FILENAME_FORMAT = "yyyy-MM-dd-HH-mm-ss-SSS"

    private var onCameraReady : OnCameraReady? = null
    private var onVideoEvent : OnVideoEvent? = null


    fun clearListeners(){
        onCameraReady = null
        onVideoEvent = null
    }


    fun changeFacingCamera(){
        lensFacing = when (lensFacing){
            CameraSelector.LENS_FACING_FRONT -> CameraSelector.LENS_FACING_BACK
            CameraSelector.LENS_FACING_BACK -> CameraSelector.LENS_FACING_FRONT
            else -> CameraSelector.LENS_FACING_BACK
        }
    }

    fun startCamera(context: Context, surfaceProvider: Preview.SurfaceProvider, lifecycleOwner: LifecycleOwner, onCameraReady: OnCameraReady) {

        CameraManager.onCameraReady = onCameraReady
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder()
                .build()
                .also {
                    it.setSurfaceProvider(surfaceProvider)
                }

            imageCapture = ImageCapture.Builder()
                .setCaptureMode(ImageCapture.CAPTURE_MODE_MINIMIZE_LATENCY)
                .build()

            // Select back camera as a default
            val cameraSelector = when (lensFacing){
                CameraSelector.LENS_FACING_BACK -> CameraSelector.DEFAULT_BACK_CAMERA
                CameraSelector.LENS_FACING_FRONT -> CameraSelector.DEFAULT_FRONT_CAMERA
                else -> CameraSelector.DEFAULT_BACK_CAMERA
            }

            try {
                // Unbind use cases before rebinding
                cameraProvider.unbindAll()

                // Bind use cases to camera
                camera = cameraProvider.bindToLifecycle(
                    lifecycleOwner, cameraSelector, preview, imageCapture
                )

                CameraManager.onCameraReady?.onCameraReady()

            } catch(exc: Exception) {
                Log.e(LOG_TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(context))
    }

    fun doesSupportFlash() : Boolean {
        return camera?.cameraInfo?.hasFlashUnit() ?: false
    }

    fun changeFlashState(){
        flashState = when (flashState){
            FlashState.on -> FlashState.off
            FlashState.off -> FlashState.auto
            FlashState.auto -> FlashState.on
        }
    }

    fun takePicture(context: Context){

        val imageCapture = imageCapture ?: return
        outputDirectory = getOutputDirectory(context)

        val photoFile = File(
            outputDirectory,
            SimpleDateFormat(
                FILENAME_FORMAT, Locale.US
            ).format(System.currentTimeMillis()) + ".jpg")

        imageCapture.flashMode = when (flashState){
            FlashState.on -> ImageCapture.FLASH_MODE_ON
            FlashState.off -> ImageCapture.FLASH_MODE_OFF
            FlashState.auto -> ImageCapture.FLASH_MODE_AUTO
        }

        val outputOptions = ImageCapture.OutputFileOptions.Builder(photoFile).build()

        imageCapture.takePicture(
            outputOptions, cameraExecutor, object : ImageCapture.OnImageSavedCallback {
                override fun onError(exc: ImageCaptureException) {
                    Log.e(LOG_TAG, "Photo capture failed: ${exc.message}", exc)
                }

                override fun onImageSaved(output: ImageCapture.OutputFileResults) {
                    val savedUri = output.savedUri ?: Uri.fromFile(photoFile)
                    addImageToGallery(context,savedUri)
                    onCameraReady?.onImageCaptured()
                }
            })
    }

    fun addImageToGallery(context: Context,savedUri : Uri){
        val mimeType = MimeTypeMap.getSingleton()
            .getMimeTypeFromExtension(savedUri.toFile().extension)
        MediaScannerConnection.scanFile(
            context,
            arrayOf(savedUri.toFile().absolutePath),
            arrayOf(mimeType)
        ) { _, uri ->
            Log.d(LOG_TAG, "Image capture scanned into media store: $uri")
        }
    }


    //VIDEO

    private lateinit var videoCapture: VideoCapture<Recorder>
    private var activeRecording: ActiveRecording? = null
    private lateinit var recordingState: VideoRecordEvent

    @SuppressLint("MissingPermission")
    fun startRecordingPreview(activity: Activity, surfaceProvider: Preview.SurfaceProvider, lifecycleOwner: LifecycleOwner) {

        val cameraProviderFuture = ProcessCameraProvider.getInstance(activity)

        cameraProviderFuture.addListener(Runnable {
            // Used to bind the lifecycle of cameras to the lifecycle owner
            val cameraProvider: ProcessCameraProvider = cameraProviderFuture.get()

            // Preview
            val preview = Preview.Builder().setTargetAspectRatio(AspectRatio.RATIO_16_9)
                .build()
                .also {
                    it.setSurfaceProvider(surfaceProvider)
                }

            val cameraSelector = when (lensFacing){
                CameraSelector.LENS_FACING_BACK -> CameraSelector.DEFAULT_BACK_CAMERA
                CameraSelector.LENS_FACING_FRONT -> CameraSelector.DEFAULT_FRONT_CAMERA
                else -> CameraSelector.DEFAULT_BACK_CAMERA
            }


            //todo add quility selector to ui
            val qualitySelector = QualitySelector
                .firstTry(QualitySelector.QUALITY_UHD)
                .thenTry(QualitySelector.QUALITY_FHD)
                .thenTry(QualitySelector.QUALITY_HD)
                .finallyTry(QualitySelector.QUALITY_SD,
                    QualitySelector.FALLBACK_STRATEGY_LOWER)

            val recorder = Recorder.Builder()
                .setQualitySelector(qualitySelector)
                .build()
            videoCapture = VideoCapture.withOutput(recorder)

            try {
                cameraProvider.unbindAll()
                cameraProvider.bindToLifecycle(
                    lifecycleOwner,
                    cameraSelector,
                    videoCapture,
                    preview
                )
            } catch (exc: Exception) {
                // we are on main thread, let's reset the controls on the UI.
                Log.e(LOG_TAG, "Use case binding failed", exc)
            }

        }, ContextCompat.getMainExecutor(activity))
    }

    @SuppressLint("MissingPermission", "UnsafeOptInUsageError")
    fun startRecording(activity: Activity, onVideoEvent: OnVideoEvent){

        CameraManager.onVideoEvent = onVideoEvent
        outputDirectory = getOutputDirectory(activity)

        outputDirectory = getOutputDirectory(activity)
        // Create time-stamped output file to hold the image
        val videoFile = File(
            outputDirectory,
            SimpleDateFormat(
                FILENAME_FORMAT, Locale.US
            ).format(System.currentTimeMillis()) + ".mp4")

        val fileOutputOptions = FileOutputOptions.Builder(videoFile).build()

        // configure Recorder and Start recording to the mediaStoreOutput.
        activeRecording =
            videoCapture.output.prepareRecording(activity, fileOutputOptions)
                .withEventListener(
                    cameraExecutor,
                    captureListener
                )
                .apply { withAudioEnabled() }
                .start()

        Log.i(LOG_TAG, "Recording started")
    }

    // todo add pause video
    fun stopRecording(){
        activeRecording?.stop()
    }


    private val captureListener = Consumer<VideoRecordEvent> { event ->
        handleRecordingEvent(event)
        if (event !is VideoRecordEvent.Status)
            recordingState = event

    }

    private fun handleRecordingEvent(event: VideoRecordEvent) {

        onVideoEvent?.onVideoEvent(event)

        when (event) {
            is VideoRecordEvent.Status -> {
            }
            is VideoRecordEvent.Start -> {
            }
            is VideoRecordEvent.Finalize -> {
            }
            is VideoRecordEvent.Pause -> {
            }
            is VideoRecordEvent.Resume -> {
            }
            else -> {
                Log.e(LOG_TAG, "Error(Unknown Event) from Recorder")
                return
            }
        }

        val stats = event.recordingStats
        val size = stats.numBytesRecorded / 1000
        val time = java.util.concurrent.TimeUnit.NANOSECONDS.toSeconds(stats.recordedDurationNanos)
        var text = "${event.toString()}: recorded ${size}KB, in ${time}second"
        if (event is VideoRecordEvent.Finalize){
            text = "${text}\nFile saved to: ${event.outputResults.outputUri}"
//            val savedUri = event.outputResults.outputUri
//            addImageToGallery(context,savedUri)
        }

        Log.i(LOG_TAG, "recording event: $text")

    }

    fun getSupportedCameras(context: Context) : SupportedCamera {
        val cameraProviderFuture = ProcessCameraProvider.getInstance(context)
        val cameraProvider = cameraProviderFuture.get()
        val frontCamera = cameraProvider.hasCamera(CameraSelector.DEFAULT_FRONT_CAMERA)
        val backCamera = cameraProvider.hasCamera(CameraSelector.DEFAULT_BACK_CAMERA)

        return if (frontCamera && backCamera){
            SupportedCamera.all
        }else if (frontCamera){
            SupportedCamera.onlyFront
        }else if (backCamera){
            SupportedCamera.onlyBack
        }else{
            SupportedCamera.none
        }
    }

    fun getOutputDirectory(context: Context): File? {
        if (outputDirectory != null) return outputDirectory
        val mediaDir = context.externalMediaDirs.firstOrNull()?.let {
            File(it, context.getString(R.string.app_name)).apply { mkdirs() } }
        return if (mediaDir != null && mediaDir.exists())
            mediaDir else null
    }

    interface OnCameraReady {
        fun onCameraReady()
        fun onImageCaptured()
    }

    interface OnVideoEvent {
        fun onVideoEvent(videoRecordEvent: VideoRecordEvent)
    }

}