package com.zaprah.rivercamera.ui.gallery

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.zaprah.rivercamera.R
import com.zaprah.rivercamera.adapters.CameraAdapter
import com.zaprah.rivercamera.databinding.GalleryFragmentBinding
import com.zaprah.rivercamera.utils.Constants.Companion.VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY

class GalleryFragment : Fragment(), CameraAdapter.OnCellClicked {


    private lateinit var viewModel: GalleryViewModel
    private lateinit var mBinding: GalleryFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = GalleryFragmentBinding.inflate(inflater)
        viewModel = ViewModelProvider(this).get(GalleryViewModel::class.java)
        setObservers()
        return mBinding.root
    }

    override fun onResume() {
        super.onResume()
        viewModel.getFilesList(lifecycleScope)
    }

    private fun setObservers(){
        viewModel.filesList.observe(viewLifecycleOwner, {files ->
            mBinding.imagesRC.adapter = CameraAdapter(files,this)
        })
    }

    override fun onCellClicked(filePath: String) {
        val bundle = Bundle()
        bundle.putString(VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY,filePath)
        Navigation.findNavController(requireActivity(), R.id.fragment_container).navigate(R.id.action_gallery_to_viewer,bundle)
    }

}