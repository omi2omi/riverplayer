package com.zaprah.rivercamera.ui.camera

import android.net.Uri
import androidx.lifecycle.ViewModelProvider
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.camera.video.VideoRecordEvent
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import com.zaprah.rivercamera.ui.main.MainActivity
import com.zaprah.rivercamera.R
import com.zaprah.rivercamera.databinding.CameraFragmentBinding
import com.zaprah.rivercamera.enums.CurrentCameraType
import com.zaprah.rivercamera.enums.FlashState
import com.zaprah.rivercamera.enums.SupportedCamera
import com.zaprah.rivercamera.extenstions.clickFade
import com.zaprah.rivercamera.extenstions.hide
import com.zaprah.rivercamera.extenstions.isVideoPath
import com.zaprah.rivercamera.extenstions.show
import com.zaprah.rivercamera.managers.CameraManager
import com.zaprah.rivercamera.utils.Constants.Companion.LOG_TAG

class CameraFragment : Fragment(), View.OnClickListener, CameraManager.OnCameraReady,
    CameraManager.OnVideoEvent {

    private lateinit var viewModel: CameraViewModel
    private lateinit var mBinding: CameraFragmentBinding
    private var currentCameraType = CurrentCameraType.image
    private var isRecording = false


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = CameraFragmentBinding.inflate(inflater)
        viewModel = ViewModelProvider(this).get(CameraViewModel::class.java)
        setObservers()
        return mBinding.root
    }

    override fun onResume() {
        super.onResume()

        updateCameraUI()
        setListeners()
        startCamera()

    }

    private fun setListeners() {
        mBinding.cameraClick.setOnClickListener(this)
        mBinding.cameraChangeBtn.setOnClickListener {
            CameraManager.changeFacingCamera()
            startCamera()
        }
        mBinding.flashBtn.setOnClickListener {
            CameraManager.changeFlashState()
            updateCameraUI()
        }

        mBinding.cameraTypeBtn.setOnClickListener {
            changeCameraType()
            startCamera()
        }

        mBinding.lastImageBtn.setOnClickListener {
            Navigation.findNavController(requireActivity(), R.id.fragment_container)
                .navigate(R.id.action_camera_to_gallery)
        }
    }

    private fun startCamera() {
        when (currentCameraType) {
            CurrentCameraType.image -> {
                CameraManager.startCamera(
                    requireContext(),
                    mBinding.previewView.surfaceProvider,
                    viewLifecycleOwner,
                    this
                )
            }
            CurrentCameraType.video -> {
                activity?.let {
                    CameraManager.startRecordingPreview(
                        it,
                        mBinding.previewView.surfaceProvider,
                        viewLifecycleOwner
                    )
                }
            }
        }
    }

    override fun onClick(p0: View?) {
        when (currentCameraType) {
            CurrentCameraType.image -> {
                context?.let {
                    CameraManager.takePicture(it)
                    mBinding.previewView.clickFade()
                }
            }
            CurrentCameraType.video -> {
                if (isRecording) {
                    activity?.let { CameraManager.stopRecording() }
                    mBinding.cameraClick.setBackgroundResource(R.mipmap.ic_camera_start_record)
                } else {
                    activity?.let { CameraManager.startRecording(it, this) }
                    mBinding.cameraClick.setBackgroundResource(R.mipmap.ic_camera_stop_record)
                }
                isRecording = isRecording.not()
            }
        }
    }

    override fun onPause() {
        super.onPause()
        mBinding.cameraClick.setOnClickListener(null)
        mBinding.flashBtn.setOnClickListener(null)
        mBinding.cameraChangeBtn.setOnClickListener(null)
        mBinding.lastImageBtn.setOnClickListener(null)
        CameraManager.clearListeners()
    }

    override fun onCameraReady() {
        updateCameraUI()
        getLastImage()
    }

    private fun updateCameraUI() {
        viewModel.getCameraDetails(requireContext())
    }

    private fun setObservers() {
        viewModel.lastImageUri.observe(viewLifecycleOwner, { fileUri ->
            setGalleryThumbnail(fileUri)
        })

        viewModel.supportedCameras.observe(viewLifecycleOwner, { supportedCamera ->
            when (supportedCamera) {
                SupportedCamera.all -> {
                    mBinding.cameraChangeBtn.show()
                }
                SupportedCamera.onlyBack,
                SupportedCamera.onlyFront -> {
                    mBinding.cameraChangeBtn.hide()
                }
                SupportedCamera.none -> {
                    Toast.makeText(requireContext(), "ERROR NO CAMERA", Toast.LENGTH_LONG).show()
                }
            }
        })

        viewModel.doesSupportFlash.observe(viewLifecycleOwner, { doesSupportFlash ->
            if (doesSupportFlash) {
                mBinding.flashBtn.show()
            } else {
                mBinding.flashBtn.hide()
            }
        })

        viewModel.currentFlashState.observe(viewLifecycleOwner, { flashState ->
            val drawable = when (flashState) {
                FlashState.on -> R.mipmap.ic_flash_on
                FlashState.off -> R.mipmap.ic_flash_off
                FlashState.auto -> R.mipmap.ic_flash_auto
            }
            mBinding.flashBtn.setBackgroundResource(drawable)
        })

        viewModel.currentCameraType.observe(viewLifecycleOwner, { currentCameraType ->
            this.currentCameraType = currentCameraType
            when (currentCameraType) {
                CurrentCameraType.video -> {
                    mBinding.cameraTypeBtn.setBackgroundResource(R.mipmap.ic_camera_video)
                    mBinding.cameraClick.setBackgroundResource(R.mipmap.ic_camera_start_record)
                    mBinding.flashBtn.hide()
                    mBinding.cameraChangeBtn.hide()
                }
                CurrentCameraType.image -> {
                    mBinding.cameraTypeBtn.setBackgroundResource(R.mipmap.ic_camera_photo)
                    mBinding.cameraClick.setBackgroundResource(R.drawable.round_background)
                }
            }
        })
    }

    private fun setGalleryThumbnail(uri: Uri) {
        Log.d(LOG_TAG, "setGalleryThumbnail ${uri.path}")
        mBinding?.lastImageBtn?.let { photoViewButton ->
            photoViewButton.post {
                // Load thumbnail into circular button using Glide
                if (uri.path?.isVideoPath() == true) {
                    Glide
                        .with(photoViewButton)
                        .asBitmap()
                        .load(uri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(photoViewButton);
                } else {
                    Glide.with(photoViewButton)
                        .load(uri)
                        .apply(RequestOptions.circleCropTransform())
                        .into(photoViewButton)
                }
            }
        }
    }

    private fun getLastImage() {
        viewModel.getLastImagePath(lifecycleScope)
    }

    override fun onVideoEvent(videoRecordEvent: VideoRecordEvent) {

        when (videoRecordEvent) {
            is VideoRecordEvent.Start -> {
                //todo add seconds view in UI
            }
            is VideoRecordEvent.Finalize -> {
                getLastImage()
            }
            is VideoRecordEvent.Pause -> {
            }
            is VideoRecordEvent.Resume -> {
            }
            else -> {
                Log.e("TEST", "Error(Unknown Event) from Recorder")
                return
            }
        }
    }

    private fun changeCameraType() {
        viewModel.changeCameraType()
    }

    override fun onImageCaptured() {
        getLastImage()
    }

}