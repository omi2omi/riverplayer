package com.zaprah.rivercamera.ui.camera

import android.app.Application
import android.content.Context
import android.net.Uri
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.MutableLiveData
import com.zaprah.rivercamera.enums.CurrentCameraType
import com.zaprah.rivercamera.enums.FlashState
import com.zaprah.rivercamera.enums.SupportedCamera
import com.zaprah.rivercamera.managers.CameraManager
import com.zaprah.rivercamera.utils.Constants
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class CameraViewModel(application: Application) : AndroidViewModel(application)  {

    private val context = application
    var lastImageUri = MutableLiveData<Uri>()
    var currentCameraType = MutableLiveData<CurrentCameraType>()
    val currentFlashState = MutableLiveData<FlashState>()
    val doesSupportFlash = MutableLiveData<Boolean>()
    val supportedCameras = MutableLiveData<SupportedCamera>()

    init {
        currentCameraType.value = CurrentCameraType.image
        currentFlashState.value = FlashState.off
        lastImageUri.value = Uri.EMPTY
        doesSupportFlash.value = true
        supportedCameras.value = SupportedCamera.all
    }

    fun getLastImagePath(lifecycleCoroutineScope: LifecycleCoroutineScope) {
        Log.d("TEST","getLastImagePath")
        lifecycleCoroutineScope.launch(Dispatchers.IO) {
            context?.let {
                CameraManager.getOutputDirectory(it)?.listFiles { file ->
                    Constants.FILE_EXTENSION_SUPPORT.contains(file.extension.toUpperCase(Locale.ROOT))
                }?.maxOrNull()?.let { file ->
                    lastImageUri.postValue(Uri.fromFile(file))
                }
            }
        }
    }

    fun getCameraDetails(context: Context){
        currentFlashState.value = CameraManager.flashState
        doesSupportFlash.value = CameraManager.doesSupportFlash()
        supportedCameras.value = CameraManager.getSupportedCameras(context)
        currentCameraType.value = currentCameraType.value
    }

    fun changeCameraType(){
        currentCameraType.value = when (currentCameraType.value){
            CurrentCameraType.video -> {
                CurrentCameraType.image
            }
            CurrentCameraType.image -> {
                CurrentCameraType.video
            }
            else -> CurrentCameraType.image
        }
    }

}