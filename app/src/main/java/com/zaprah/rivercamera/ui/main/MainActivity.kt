package com.zaprah.rivercamera.ui.main

import android.content.Context
import android.content.pm.PackageManager
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.core.content.ContextCompat
import androidx.core.view.WindowCompat
import androidx.core.view.WindowInsetsCompat
import androidx.core.view.WindowInsetsControllerCompat
import com.zaprah.rivercamera.databinding.MainActivityBinding
import com.zaprah.rivercamera.utils.Constants
import com.zaprah.rivercamera.utils.Constants.Companion.PERMISSIONS_REQUEST_CODE
import com.zaprah.rivercamera.utils.Constants.Companion.PERMISSIONS_REQUIRED

class MainActivity : AppCompatActivity() {

    private lateinit var activityMainBinding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        activityMainBinding = MainActivityBinding.inflate(layoutInflater)
        setContentView(activityMainBinding.root)
    }


    override fun onResume() {
        super.onResume()
        if (hasPermissions(applicationContext).not()){
            requestPermissions(PERMISSIONS_REQUIRED, PERMISSIONS_REQUEST_CODE)
        }
    }

    override fun onWindowFocusChanged(hasFocus: Boolean) {
        super.onWindowFocusChanged(hasFocus)
        if (hasFocus) hideSystemUI()
    }

    private fun hideSystemUI() {
        WindowCompat.setDecorFitsSystemWindows(window, false)
        WindowInsetsControllerCompat(window, activityMainBinding.fragmentContainer).let { controller ->
            controller.hide(WindowInsetsCompat.Type.systemBars())
            controller.systemBarsBehavior = WindowInsetsControllerCompat.BEHAVIOR_SHOW_TRANSIENT_BARS_BY_SWIPE
        }
    }

    fun hasPermissions(context: Context) = Constants.PERMISSIONS_REQUIRED.all {
        ContextCompat.checkSelfPermission(context, it) == PackageManager.PERMISSION_GRANTED
    }

}