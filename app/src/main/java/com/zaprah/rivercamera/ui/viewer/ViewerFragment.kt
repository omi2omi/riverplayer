package com.zaprah.rivercamera.ui.viewer

import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.MediaController
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.zaprah.rivercamera.databinding.ViewerFragmentBinding
import com.zaprah.rivercamera.extenstions.isVideoPath
import com.zaprah.rivercamera.utils.Constants.Companion.VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY
import java.io.File

class ViewerFragment : Fragment() {

    var path: String? = null

    private lateinit var mBinding: ViewerFragmentBinding

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        mBinding = ViewerFragmentBinding.inflate(inflater)
        path = arguments?.getString(VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY)
        return mBinding.root
    }

    override fun onResume() {
        super.onResume()
        startView()
    }

    override fun onPause() {
        super.onPause()
        stopVideo()
    }

    private fun startView() {
        if (path?.isVideoPath() == true) {
            startVideo()
        } else {
            startPhotoPreview()
        }
    }


    //todo change to exo player, handle audio focus
    private fun startVideo() {
        mBinding.photoViewer.visibility = View.GONE
        mBinding.videoViewer.visibility = View.VISIBLE
        val mc = MediaController(requireContext())
        mc.setPadding(0,0,0,200)
        mBinding.videoViewer.apply {
            setVideoURI(Uri.fromFile(File(path)))
            setMediaController(mc)
            requestFocus()
        }.start()
    }


    private fun startPhotoPreview() {
        mBinding.photoViewer.visibility = View.VISIBLE
        mBinding.videoViewer.visibility = View.GONE
        Glide.with(requireContext())
            .load(Uri.fromFile(File(path)))
            .into(mBinding.photoViewer)
    }


    private fun stopVideo(){
        mBinding.videoViewer.stopPlayback()
    }

}