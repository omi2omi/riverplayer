package com.zaprah.rivercamera.ui.gallery

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LifecycleCoroutineScope
import androidx.lifecycle.MutableLiveData
import com.zaprah.rivercamera.managers.CameraManager
import com.zaprah.rivercamera.utils.Constants.Companion.FILE_EXTENSION_SUPPORT
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import java.util.*

class GalleryViewModel(application: Application) : AndroidViewModel(application) {

    private val context = application
    var filesList = MutableLiveData<Array<String>>()

    init {
        filesList.value = emptyArray()
    }

    fun getFilesList(lifecycleCoroutineScope: LifecycleCoroutineScope) {
        lifecycleCoroutineScope.launch(Dispatchers.IO) {
            context.let {
                CameraManager.getOutputDirectory(it)?.listFiles { file ->
                    FILE_EXTENSION_SUPPORT.contains(file.extension.toUpperCase(Locale.ROOT))
                }?.let { files ->
                    val fileList = mutableListOf<String>()
                    files?.sortBy { it.lastModified() }
                    files?.forEach { file ->
                        fileList.add(file.absolutePath)
                    }
                    if (fileList.isNotEmpty()) {
                        filesList.postValue(fileList.reversed().toTypedArray())
                    }

                }
            }
        }
    }
}