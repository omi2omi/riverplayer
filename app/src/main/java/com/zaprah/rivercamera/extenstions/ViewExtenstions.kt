package com.zaprah.rivercamera.extenstions

import android.animation.Animator
import android.view.View
import com.zaprah.rivercamera.utils.Constants.Companion.CLICK_ANIMATION_DURATION
import com.zaprah.rivercamera.utils.Constants.Companion.VIEW_ANIMATION_DURATION

fun View.clickFade() {
    this.alpha = 1f
    this.animate().alpha(0f).setDuration(CLICK_ANIMATION_DURATION).setListener(object : Animator.AnimatorListener{
        override fun onAnimationStart(animation: Animator?) {
        }

        override fun onAnimationEnd(animation: Animator?, isReverse: Boolean) {
            super.onAnimationEnd(animation, isReverse)
        }

        override fun onAnimationEnd(animation: Animator?) {
            this@clickFade.animate().alpha(1f).setDuration(CLICK_ANIMATION_DURATION).start()
        }

        override fun onAnimationCancel(animation: Animator?) {}

        override fun onAnimationRepeat(animation: Animator?) {}
    }).start()
}


fun View.hide() {
    this.alpha = 1f
    this.animate().alpha(0f).setDuration(VIEW_ANIMATION_DURATION).withEndAction { this@hide.visibility = View.INVISIBLE }.start()
}

fun View.show() {
    this.alpha = 0f
    this.visibility = View.VISIBLE
    this.animate().alpha(1f).setDuration(VIEW_ANIMATION_DURATION).start()
}