package com.zaprah.rivercamera.extenstions

fun String.isVideoPath() : Boolean{
    return this.endsWith(".mp4",true)
}