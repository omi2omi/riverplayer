package com.zaprah.rivercamera.utils

import android.Manifest

class Constants {

    companion object {
        const val CLICK_ANIMATION_DURATION = 50L
        const val VIEW_ANIMATION_DURATION = 50L
        val FILE_EXTENSION_SUPPORT = arrayListOf(
            "jpg",
            "JPG",
            "mp4",
            "MP4"
        )
        const val LOG_TAG = "!!TEST!!"

        val PERMISSIONS_REQUIRED = arrayOf(Manifest.permission.CAMERA,Manifest.permission.RECORD_AUDIO)
        const val PERMISSIONS_REQUEST_CODE = 1234

        const val VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY = "VIEWER_FRAGMENT_BUNDLE_FILE_PATH_KEY"

    }
}