package com.zaprah.rivercamera.enums

enum class CurrentCameraType {
    image,
    video
}