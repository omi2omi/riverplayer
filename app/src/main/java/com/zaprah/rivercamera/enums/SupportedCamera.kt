package com.zaprah.rivercamera.enums

enum class SupportedCamera {
    onlyFront,
    onlyBack,
    all,
    none
}