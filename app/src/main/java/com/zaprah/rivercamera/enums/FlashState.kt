package com.zaprah.rivercamera.enums

enum class FlashState {
    on,
    off,
    auto
}