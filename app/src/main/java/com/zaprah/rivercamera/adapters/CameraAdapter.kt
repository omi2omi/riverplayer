package com.zaprah.rivercamera.adapters

import android.media.MediaMetadataRetriever
import android.net.Uri
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.zaprah.rivercamera.R
import com.zaprah.rivercamera.extenstions.isVideoPath
import java.io.File

class CameraAdapter (private val dataSet: Array<String>, var onCellClicked: OnCellClicked) : RecyclerView.Adapter<CameraAdapter.ViewHolder>() {

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val backgroundImage: ImageView = view.findViewById(R.id.cellBackground)
        val playIcon :ImageView = view.findViewById(R.id.cellPlayIcon)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.camera_item_cell, viewGroup, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(viewHolder: ViewHolder, position: Int) {

        val path = dataSet[position]

        viewHolder.backgroundImage.setOnClickListener {
            onCellClicked?.onCellClicked(dataSet[position])
        }

        val uri = Uri.fromFile(File(path))
        if (path.isVideoPath()){

            //todo add duration to video cells
            val retriever = MediaMetadataRetriever()
            retriever.setDataSource(viewHolder.itemView.context, uri)
            val duration = retriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_DURATION)
            retriever.release()

            Log.d("TEST", "File duration = ${duration?.toLongOrNull() ?: 0}")

            viewHolder.playIcon.visibility = View.VISIBLE
            Glide
                .with(viewHolder.backgroundImage)
                .asBitmap()
                .load(uri)
                .into(viewHolder.backgroundImage)
        } else {
            viewHolder.playIcon.visibility = View.GONE
            Glide.with(viewHolder.backgroundImage)
                .load(uri)
                .into(viewHolder.backgroundImage)
        }
    }

    // Return the size of your dataset (invoked by the layout manager)
    override fun getItemCount() = dataSet.size

    interface OnCellClicked {
        fun onCellClicked(filePath : String)
    }

}